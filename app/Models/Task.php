<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

	protected $table = 'tasks';
	protected $fillable = ['title','description','done'];

	public static function addTask($input)
	{
		$response = array();

		$task = Task::create($input);
		
		$response['message'] = 'Task created';
		$response['error'] = false;
		$response['data'] = $task;

		return $response;
	}

	public static function getOrderedTasks()
	{
		$tasks = \DB::table('tasks')
			->orderBy('done','ASC')
			->orderBy('created_at','ASC')
			->get();
		return $tasks;
	}

}
