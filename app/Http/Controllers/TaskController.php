<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tasks = Task::getOrderedTasks();
		return view('tasks.index', array('tasks' => $tasks));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$task = new Task;
		return view('tasks.create', array('task' => $task));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = $request->only('title','description','done');

		// if(isset($input->done)){
		// 	$input->done = '1';
		// }else{
		// 	$input->done = '0';
		// }

		$response = Task::addTask($input);

		if($response['error'] == true){
			return redirect('task/create')->withErrors($response['message'])->withInput();
		}else{
			return redirect('task'); 
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$task = Task::find($id);
		return view('tasks.show', array('task' => $task));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$task = Task::find($id);
		return view('tasks.edit', array('task' => $task));
		// return view('tasks.edit', ['id' => $id]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$task = Task::find($id);
		$task->title = $request->title;
		$task->description = $request->description;
		$task->done = $request->done;
		$task->save();
		return redirect('task/'.$id); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$task = Task::find($id);
		$task->delete();
		return redirect('/');
	}

}
