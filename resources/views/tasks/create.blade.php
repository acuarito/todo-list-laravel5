@extends('layout')
@section('content')
<div class="col-md-6 col-md-offset-3">
	<div class="page-header"><h1>Create a task</h1></div>
	<div class="panel-body">
		{!! Form::open(array('route' => 'task.store', 'class' => 'form-horizontal')) !!}
			
			@include('tasks.form')

		{!! Form::close() !!}
	</div>
</div>
@endsection