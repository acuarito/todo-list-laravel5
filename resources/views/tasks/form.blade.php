<div class="form-group">
	<label for="title">Title</label>
	{!! Form::text('title', $task->title, $attributes = array('class' => 'form-control')) !!}
</div>
<div class="form-group">
	<label for="description">Description</label>
	{!! Form::textarea('description', $task->description, $attributes = array('class' => 'form-control')) !!}
</div>
<div class="form-group">
	<label for="status">Status: </label>
	{!! Form::select('done', array(0 => 'Pending', 1 => 'Done'), $task->done) !!}
</div>
<!-- <div class="checkbox">
	<label for="status">
		{!! Form::checkbox('done', $task->done) !!} Done
	</label>
</div> -->
<!-- <br> -->
<div class="form-group">
	{!! Form::submit('Send', $attributes = array('class' => 'btn btn-primary')) !!}
	<a href=" {{ route('task.index') }} " class="btn btn-default" role="button">Back to Index</a>
</div>