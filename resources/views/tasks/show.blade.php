@extends('layout')

@section('content')
<div class="col-md-6 col-md-offset-3">
	<div class="page-header"><h1>Task description</h1></div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"> {{ $task->title }} </h3>
		</div>
		<div class="panel-body">
			{{ $task->description }}
		</div>
	</div>
	<div class="form-group">
		Status:
		@if($task->done == true)
			<span class="label label-success">Done</span>
		@else
			<span class="label label-warning">Pending</span>
		@endif
	</div>

	<a href=" {{ route('task.edit', $task->id) }} " class="btn btn-primary btn-block" role="button">Edit</a>

	{!! Form::open(array('action' => array('TaskController@destroy', $task->id), 'method' => 'DELETE')) !!}
		{!! Form::submit('Delete', $attributes = array('class' => 'btn btn-danger btn-block')) !!}
	{!! Form::close() !!}

	<a href=" {{ route('task.index') }} " class="btn btn-default btn-block" role="button">Back to Index</a>
	
</div>
@endsection