@extends('layout')
@section('content')
<div class="col-md-6 col-md-offset-3">
	<div class="page-header"><h1>Edit task</h1></div>
	<div class="panel-body">
		{!! Form::open(array('action' => array('TaskController@update', $task->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}
			
			@include('tasks.form',$task)

		{!! Form::close() !!}
	</div>
</div>
@endsection