@extends('layout')
@section('content')
<div class="col-md-10 col-md-offset-1">
	<div class="page-header"><h1>Index</h1></div>

	<a href="task/create" class="btn btn-default" ole="button">Create task</a><br><br>

	<div class="panel panel-default">
		<div class="panel-heading"><h4>List of tasks</h4></div>
		<!-- <div class="panel-body"> -->
		<table class="table table-hover">
			<thead><tr>
				<th>Title</th>
				<th>Status</th>
				<th>Created at</th>
				<th>Updated at</th>
				<th></th>
			</tr></thead>
			<tbody>
			@foreach($tasks as $t)
		    	<tr>
		    		<td> {{ $t->title }} </td>
		    		@if($t->done == true)
		    			<td> Done </td>
		    		@else
		    			<td>Pending</td>
		    		@endif
		    		<td> {{ $t->created_at }} </td>
		    		<td> {{ $t->updated_at }}</td>
		    		<td> <a href="{{ route('task.show', $t->id) }}">Show</a> </td>
		    		<!--<td> <a href="{{ route('task.destroy', $t->id, ['method' => 'DELETE']) }}">Delete</a> </td>-->
		    	</tr>
		  	@endforeach
			</tbody>
		</table>
		<!-- </div> -->
	</div>
	<script type="text/javascript">
		$('.btn-default').click(function(){
			alert('asd');
		});
	</script>
</div>
@endsection